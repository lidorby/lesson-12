#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include <queue>
#include <Windows.h>
#include <mutex>
#include <condition_variable>

class MessageSender
{
	public:
		MessageSender();
		~MessageSender();
		void printMenu();
		void sendMessages();
		void readFile();
	private:
		void signin();
		void signout();
		std::vector<std::string>* connectedUsers;
		std::queue<std::string>* messages;
		std::vector<std::string> getUsers();
		void print_users();
		bool checkIfExists(std::string name);
};