#include <iostream>
#include "MessageSender.h"
#include <thread>

int main()
{
	MessageSender MsgSender;
	std::thread first(&MessageSender::printMenu, MsgSender);
	std::thread sec(&MessageSender::readFile, MsgSender);
	std::thread third(&MessageSender::sendMessages, MsgSender);
	first.join();
	sec.detach();
	third.detach();

	return 0;
}