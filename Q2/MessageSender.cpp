#include "MessageSender.h"

std::mutex signMtx; //sing in/out lock
std::mutex dataMtx; //lines lock
std::condition_variable conData; 


MessageSender::MessageSender()//ctor
{
	this->connectedUsers = new std::vector<std::string>;
	this->messages = new std::queue<std::string>;
}

MessageSender::~MessageSender()//dtor
{
	if (!this->connectedUsers && !this->messages)
	{
		delete this->connectedUsers;
		delete this->messages;
	}
}

void MessageSender::printMenu()
{
	int choice = 0;

	while (choice != 4)//this one prints the menu and asks for a choice
	{
		system("cls");
		std::cout << "Welcome to my messages sender, please make a choice!\n1.sign in\n2.sign out\n3.see all connected users\n4.exit" << std::endl;
		std::cin >> choice;
		system("cls");
		switch (choice)
		{
			case 1:
			{
				this->signin();
				break;
			}
			case 2:
			{
				this->signout();
				break;
			}
			case 3:
			{
				this->print_users();
				break;
			}
			case 4:
			{
				std::cout << "Seeya" << std::endl;
				getchar();
				break;
			}
			default:
			{
				std::cout << "invalid!!! dont mess with the system" << std::endl;
				break;
			}
		}
	}
}

void MessageSender::sendMessages()//this function sends all the messages fr
{
	std::ofstream msgF;
	std::string Msg;
	while (true)//adds all the messages to the output
	{
		std::unique_lock<std::mutex> dataLock(dataMtx);
		conData.wait(dataLock, [=]() {return !(*this->messages).empty(); });
		msgF.open("output.txt", std::ofstream::out | std::ofstream::app);
		if (msgF)
		{
			Msg = this->messages->front();
			this->messages->pop();
			dataLock.unlock();

			for (auto user : *this->connectedUsers)//This is like python
			{
				msgF << user << ": " << Msg << std::endl;//sends every message
			}
			msgF.close();
		}
	}
}

std::vector<std::string> MessageSender::getUsers()//returns all users
{
	return *this->connectedUsers;
}

void MessageSender::signin()//sing in func
{
	std::string UserN;
	std::vector<std::string> users = getUsers();
	std::cout << "Enter UserName: ";
	std::cin >> UserN;//gets data
	if (checkIfExists(UserN))//checks if the user exist
	{
		std::cout << "this username is taken..." << std::endl;
		getchar();//buffer stuff...
		getchar();
	}
	else//if the user not exist, add him
	{
		std::unique_lock<std::mutex> conLock(signMtx);
		this->connectedUsers->push_back(UserN);
	}
}


void MessageSender::signout()
{
	std::string UserN;
	std::cout << "Enter UserName to erase: ";
	std::cin >> UserN;
	if (checkIfExists(UserN))//if the user exists, sing him out
	{
		std::unique_lock<std::mutex> conLock(signMtx);
		(*this->connectedUsers).erase(std::remove((*this->connectedUsers).begin(), (*this->connectedUsers).end(), UserN), (*this->connectedUsers).end());
		std::cout << "this user is now signed out" << std::endl;
		getchar();
	}
	else//if not , print message to the user that it doesnt exist
	{
		std::cout << "user not found, non of the users have singed out" << std::endl;
		getchar();
	}
	getchar();
}

void MessageSender::print_users()//prints all the users
{
	int count = 0;
	for (auto name : *this->connectedUsers)//works like python
	{
		std::cout << count << "-> " << name << std::endl;
		count++;
	}
	getchar();//readline
}

//this function gets a name and checks if he is exists in the list of usernames
bool MessageSender::checkIfExists(std::string name)
{
	for (auto user : *this->connectedUsers)//This is like python
	{
		if (user.data() == name)
		{
			return true;
		}
	}
	return false;
}

void MessageSender::readFile()
{
	std::string line;
	std::ifstream DataF;
	while (true)
	{	
		std::this_thread::sleep_for(std::chrono::milliseconds(20000));
		DataF.open("data.txt");
		if (DataF)
		{
			while (getline(DataF, line))//this wile loops work on every line by itself
			{
				std::unique_lock<std::mutex> dataLock(dataMtx);
				this->messages->push(line);
				dataLock.unlock();
				conData.notify_one();
			}
		}
		DataF.close();
		DataF.open("data.txt", std::ofstream::out | std::ofstream::trunc);//clearing the file
		DataF.close();
	}
	
}
