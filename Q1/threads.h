#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <time.h>
#include <thread>
#include <mutex>


using namespace std;


void I_Love_Threads();
void call_I_Love_Threads();

void printVector(vector<int> primes);

void getPrimes(int begin, int end, vector<int>& primes);
vector<int> callGetPrimes(int begin, int end);
bool ifPrime(int n);


void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);
