#include "threads.h"
std::mutex mtx;
//A FUNCTIONS

void I_Love_Threads()
{
	cout << "i love threads" << endl;
}

void call_I_Love_Threads()
{
	thread use(I_Love_Threads);
	use.join();
}
//B FUNCTIONS

void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = 0;
	for (i = begin; i <= end; i++)
	{
		if (ifPrime(i))
		{
			primes.push_back(i);
		}
	}
}

void printVector(std::vector<int> primes)
{
	int i = 0;
	for (i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> idk;
	const clock_t begin_time = clock();
	thread use(getPrimes, begin, end, ref(idk));
	use.join();
	cout << float(clock() - begin_time) / CLOCKS_PER_SEC << " seconds" << endl;
	return idk;
}

bool ifPrime(int n)
{
	for (int i = 2; i <= n / 2; ++i)
	{
		if (n % i == 0)
		{
			return false;
		}
	}
	return true;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	int i = 0;
	for (i = begin; i <= end; i++)
	{
		mtx.lock();
		if (ifPrime(i))
		{
			file << i << ",";
		}
		mtx.unlock();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream myfile;
	myfile.open(filePath);
	vector<thread> _threads;
	int difference = (end - begin) / N;
	int _end = begin + difference;
	int _begin = begin;
	const clock_t begin_time = clock();
	for (int i = 0; i < N; i++)
	{
		_threads.push_back(thread(writePrimesToFile, _begin, _end, ref(myfile)));
		_begin = _end;
		_end += difference;

	}
	for (int i = 0; i < N; i++)
	{
		_threads[i].join();
	}
	cout << float(clock() - begin_time) / CLOCKS_PER_SEC << " seconds" << endl;
	myfile.close();
}
